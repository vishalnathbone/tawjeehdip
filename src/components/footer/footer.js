import React, {Component} from 'react';
import './footer.scss';
class Footer extends Component{

    render(){

        return(
<div className="footer">
    <div className="container">
            <div className="row">
                <div className="col-md-2">
                    <h3>Quick Links </h3>
                    <ul>
                        <li>Home</li>
                        <li>About</li>
                        <li>Services</li>
                        <li>Booking</li>
                    </ul>
                </div>
                <div className="col-md-3">
                <h3>Contacts</h3>
                <p>Phone: +971 (0) 4 9404 000<br/>
                Fax: +971 (0) 4 9304 077<br/>
                Email: infomation@gmail.com</p>
                </div>
                <div className="col-md-3">
                <h3>Subscribe to our Newsletter to get latest updates.</h3>

                <form>
                <div className="form-group row">
                   
                    <div className="col-sm-12">
                    <input type="email" className="form-control" id="inputemail" placeholder="Email" />
                    </div>
                </div>
                

                <div className="form-group row">
                    <div className="col-sm-10">
                    <button type="submit" className="btn">Subscribe</button>
                    </div>
                </div>
                </form>

                </div>
                <div className="col-md-4">
                <h3>Discover</h3>
                <div className="map">
                <iframe src="https://maps.google.com/maps?q=tawjeeh%20center%20dubai%20dip&amp;t=m&amp;z=11&amp;output=embed&amp;iwloc=near" aria-label="tawjeeh center dubai dip"></iframe>
                </div>
                </div>
            </div>
    </div>
</div>

        )
    }
}

export default Footer;