import React, {Component} from 'react';
import './navbar.scss';

class Navbar extends Component{

    render(){

        return(
            <div className="navbar">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-md-10 col-lg-10">
                            <ul>
                                <li><a href="" className="active">Home</a></li>
                                <li><a href="">Booking</a></li>
                                <li><a href="">Services</a></li>
                                <li><a href="">About</a></li>
                                <li><a href="">Contact</a></li>
                                
                            </ul>
                            <div className="clearboth"></div>
                        </div>
                        <div className="col-md-2 col-lg-2 booknow mx-auto text-center">
                            <a href="">Book Now</a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Navbar;