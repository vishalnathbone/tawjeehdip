import React, {Component} from 'react';
import Popup from "reactjs-popup";
import './booknow.scss'
class Booknowpopup extends Component{
    state = {
        name: '',
        email:'',
        mobile:'',
        company:'',
        address:'',
        bookingdate:'',
        bookingtime:'',
        message:''

      };
    constructor(props){
        super(props)
     
        
    }

    onMessageChange = event =>
    this.setState({
        message: event.target.value
    });

    onBookingTimeChange = event =>
    this.setState({
        bookingtime: event.target.value
    });

    onBookingDateChange = event =>
    this.setState({
        bookingdate: event.target.value
    });

    onAddressChange = event =>
    this.setState({
        address: event.target.value
    });

    onCompanyChange = event =>
    this.setState({
        company: event.target.value
    });

    onMobileChange = event =>
    this.setState({
      mobile: event.target.value
    });

    onNameChange = event =>
    this.setState({
      name: event.target.value
    });

    onEmailChange = event =>
    this.setState({
      email: event.target.value
    });

      
    render(){

        return(
            <div className="booknow">
            <Popup trigger={<button className="button btn btn-common btn-effect"> Book Now </button>} modal>
    {close => (
      <div className="modal">
        <a className="close" onClick={close}>
          &times;
        </a>
        <div className="header"> Booking Form </div>
        <div className="content">
        <div className="contact-block">
                <form id="contactForm">
                  <div className="row no-gutters">
                    <div className="col-12 col-md-6">
                      <div className="form-group">
                        <input type="text" className="form-control" id="name" name="name" placeholder="Your Name" required data-error="Please enter your name" onChange={this.onNameChange} />
                        <div className="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div className="col-12 col-md-6">
                      <div className="form-group">
                        <input type="text" placeholder="Your Email" id="email" className="form-control" name="email" required data-error="Please enter your email" onChange={this.onEmailChange} />
                        <div className="help-block with-errors"></div>
                      </div> 
                    </div>

                    <div className="col-12 col-md-6">
                      <div className="form-group">
                        <input type="text" className="form-control" id="mobile" name="mobile" placeholder="Your Mobile No." required data-error="Please enter mobile no." onChange={this.onMobileChange} />
                        <div className="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div className="col-12 col-md-6">
                      <div className="form-group">
                        <input type="text" placeholder="Your company" id="company" className="form-control" name="company" onChange={this.onCompanyChange} />
                        
                      </div> 
                    </div>
                    <div className="col-12 col-md-12">
                    <div className="form-group"> 
                        <textarea className="form-control" id="address" name="address" placeholder="Your address" rows="3" data-error="Enter your address" onChange={this.onAddressChange}></textarea>
                       
                      </div>
                    </div>

                    <div className="col-12 col-md-6">
                      <div className="form-group">
                        <input type="date" className="form-control" id="bookingdate" name="bookingdate" placeholder="Your Booking Date" required data-error="Please enter your date" onChange={this.onBookingDateChange} />
                        <div className="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div className="col-12 col-md-6">
                      <div className="form-group">
                        <select className="form-control" name="bookingtime" id="bookingtime" required data-error="Please enter your time" onChange={this.onBookingTimeChange}>
                        <option>Select Time</option>
                        <option value="Morning (8AM-12PM)">Morning (8AM-12PM)</option>
                        <option value="Afternoon (12PM-3PM)">Afternoon (12PM-3PM)</option>
                        <option value="Evening (3PM-5:30PM)">Evening (3PM-5:30PM)</option>
                       
                        </select>
                        <div className="help-block with-errors"></div>
                      </div> 
                    </div>

                    <div className="col-md-12">
                      <div className="form-group"> 
                        <textarea className="form-control" id="message" name="message" placeholder="Your Message" rows="3" data-error="Write your message" required onChange={this.onMessageChange}></textarea>
                        <div className="help-block with-errors"></div>
                      </div>
                      <div className="submit-button">
                        <button className="btn btn-common btn-effect" id="submit" type="button" onClick={this.saveBooking}>Send</button>
                        <button
            className="btn btn-common"
            onClick={() => {
              console.log('modal closed ')
              close()
            }}
          >
            close
          </button>
                        <div id="msgSubmit" className="h3 hidden"></div> 
                        <div className="clearfix"></div> 
                      </div>
                    </div>
                  </div>            
                </form>
              </div>
        </div>
      
      </div>
    )}
  </Popup>
  </div>
        )
    }

    saveBooking = () => {
        console.log(this.state)
    }
}

export default Booknowpopup;