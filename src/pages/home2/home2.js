import React,{Component} from 'react';
import Booknowpopup from '../../components/booknow-popup/booknow-popup'
class Home2 extends Component{

    render(){

        return(
            <div className="home2">
                {/* Header Section Start */}
    <header id="slider-area">  
      <nav className="navbar navbar-expand-md fixed-top scrolling-navbar bg-white">
        <div className="container">          
          <a className="navbar-brand" href="/"><img src='img/logo.png' /></a>
          <a className="logo2" href="/"><img src='images/tawjeeh.svg' /></a>
          <a className="logo3" href="/"><img src='images/mhre.svg' /></a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i className="lni-menu"></i>
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <ul className="navbar-nav mr-auto w-100 justify-content-end">
              <li className="nav-item">
                <a className="nav-link page-scroll" href="#slider-area">Home</a>
              </li>
              <li className="nav-item">
                <a className="nav-link page-scroll" href="#about-us">About Us</a>
              </li>
              <li className="nav-item">
                <a className="nav-link page-scroll" href="#services">Services</a>
              </li>  
              <li className="nav-item">
                <a className="nav-link page-scroll" href="#contactus">Contact Us</a>
              </li>
              <li className="nav-item">
                  <Booknowpopup></Booknowpopup>
                  
              </li>
                                       
              {/* <li className="nav-item">
                <a className="nav-link page-scroll" href="#portfolios">Works</a>
              </li>            
              <li className="nav-item">
                <a className="nav-link page-scroll" href="#pricing">Pricing</a>
              </li> 
              <li className="nav-item">
                <a className="nav-link page-scroll" href="#team">Team</a>
              </li>    
              <li className="nav-item">
                <a className="nav-link page-scroll" href="#subscribe">Subscribe</a>
              </li>
              <li className="nav-item">
                <a className="nav-link page-scroll" href="#blog">Blog</a>
              </li> 
              <li className="nav-item">
                <a className="nav-link page-scroll" href="#contact">Contact</a>
              </li>  */}
            </ul>              
          </div>
        </div>
      </nav> 

      {/* Main Carousel Section */}
      <div id="carousel-area">
        <div id="carousel-slider" className="carousel slide carousel-fade" data-ride="carousel">
          <ol className="carousel-indicators">
            <li data-target="#carousel-slider" data-slide-to="0" className="active"></li>
            <li data-target="#carousel-slider" data-slide-to="1"></li>
            <li data-target="#carousel-slider" data-slide-to="2"></li>
          </ol>
          <div className="carousel-inner" role="listbox">
            <div className="carousel-item active">
              <img src="img/slider/1.jpg" alt="" />
              <div className="carousel-caption text-center">
                {/* <h3 className="wow fadeInRight" data-wow-delay="0.2s">Handcrafted</h3>   */}
                <h2 className="wow fadeInRight" data-wow-delay="0.4s">Largest Tawjeeh in UAE</h2>
                {/* <h4 className="wow fadeInRight" data-wow-delay="0.6s">Comes with All Essential Sections & Elements</h4> */}
                {/* <a href="#" className="btn btn-lg btn-common btn-effect wow fadeInRight" data-wow-delay="0.9s">Book Now</a> */}
                {/* <a href="#" className="btn btn-lg btn-border wow fadeInRight" data-wow-delay="1.2s">Get Started!</a> */}
              </div>
            </div>
            <div className="carousel-item">
              <img src="img/slider/2.jpg" alt="" />
              <div className="carousel-caption text-center">
              <h2 className="wow bounceIn" data-wow-delay="0.6s">Large group handling facility</h2> 
              </div>
              {/* <div className="carousel-caption text-center">
                <h3 className="wow fadeInDown" data-wow-delay="0.3s">Bundled With Tons of</h3>
                <h2 className="wow bounceIn" data-wow-delay="0.6s">Cutting-edge Features</h2> 
                <h4 className="wow fadeInUp" data-wow-delay="0.9s">Parallax, Video, Product, Premium Addons and More...</h4>
                <a href="#" className="btn btn-lg btn-common btn-effect wow fadeInUp" data-wow-delay="1.2s">View Works</a>
              </div> */}
            </div>
            <div className="carousel-item">
              <img src="img/slider/3.jpg" alt="" />
              <div className="carousel-caption text-center">
              <h2 className="wow fadeInRight" data-wow-delay="0.6s">Mandatory training to proceed for processing working visa to your employees</h2> 
              </div>
              {/* <div className="carousel-caption text-center">
                <h3 className="wow fadeInDown" data-wow-delay="0.3s">Ready For</h3>
                <h2 className="wow fadeInRight" data-wow-delay="0.6s">Multi-purpose Websites</h2> 
                <h4 className="wow fadeInUp" data-wow-delay="0.6s">App, Business, SaaS and Landing Pages</h4>
                <a href="#" className="btn btn-lg btn-common wow fadeInUp" data-wow-delay="0.9s">Book Now</a>
              </div> */}
            </div>
          </div>
          <a className="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">
            <span className="carousel-control" aria-hidden="true"><i className="lni-chevron-left"></i></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">
            <span className="carousel-control" aria-hidden="true"><i className="lni-chevron-right"></i></span>
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>  

    </header>
    {/* Header Section End */} 



 {/* about Section Start */}
 <section id="about-us" className="section">
      <div className="container">
        <div className="section-header">          
          <h2 className="section-title">Guidance and awareness training for employees</h2>
          <span>About</span>
          <p className="section-subtitle hide">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos debitis.</p>
        </div>
        <div className="row">
          {/* Start featured */}
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="featured-box">
              <div className="featured-icon">
                <i className="lni-layout"></i>
              </div>
              <div className="featured-content">
                <div className="icon-o"><i className="lni-layout"></i></div>
                <h4>Tawjeeh</h4>
                <p>Tawjeeh is a semi- government institution that belongs to Ministry of Human Resources & Emiratisation (MOHRE) which was developed according to thestrategy of Emirates Programme for Excellence in Government Services to ensure high standards in the labor laws.</p>
              </div>
            </div>
          </div>
          {/* End featured */}
          {/* Start featured */}
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="featured-box">
              <div className="featured-icon">
                <i className="lni-tab"></i>
              </div>
              <div className="featured-content">
                <div className="icon-o"><i className="lni-tab"></i></div>
                <h4>Facilities</h4>
                <p>Tawjeeh centers provide various services above which the most important serviceis guidance lectures for both employees and employers. The lectures are conducted in a uniform manner in multi-language with distribution of brochures and pamphlets.</p>
              </div>
            </div>
          </div>
          {/* End featured */}
          {/* Start featured */}
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="featured-box">
              <div className="featured-icon">
                <i className="lni-rocket"></i>
              </div>
              <div className="featured-content">
                <div className="icon-o"><i className="lni-rocket"></i></div>
                <h4>Legal advisor</h4>
                <p>Center is well equipped with highly trained professionals to provide all required information on labor laws and regulations, submit and renew work permits, issuance of permits and labor contracts, typing new job offers.</p>
              </div>
            </div>
          </div>
          {/* End featured */}
         
        </div>
      </div>
    </section>
    {/* about Section End */}  

 {/* Call to Action Start */}
 <section className="call-action section">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-10">
            <div className="cta-trial text-center">
              <h3>Do you know the mandatory labor law in UAE ?</h3>
              <p>All employees has to go through the mandatory training from Tawjeeh</p>
              <Booknowpopup></Booknowpopup>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/* Call to Action End */}

    {/* Services Section Start */}
    <section id="services" className="section">
      <div className="container">
        <div className="section-header">          
          <h2 className="section-title">Our Services</h2>
          <span>Services</span>
          <p className="section-subtitle">We in turn in ALOUFOUK TAWJEEH CENTER (ALMURAQQABAT) are aiming at raising the awareness regarding the importance of protecting the rights and interests of employers and employees, introducing them to raise awareness  among of them about their albor rights and obligations toward the work.</p>
        </div>

        <div className="row">
                <div className="col-lg-12 col-md-12 col-xs-12">
					<div className="sub-title text-center">
						<h3>Main Services</h3>
						
					</div>
                </div>

				 <div className="col-lg-6 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="0.2s">
              <div className="icon color-1">
                <i className="lni-pencil"></i>
              </div>
              <h4>Submit new electronic work permit and mission electronic work application</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
          <div className="col-lg-6 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="0.4s">
              <div className="icon color-2">
                <i className="lni-cog"></i>
              </div>
              <h4>Guidance lecture for workers</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>

        </div>
        <div className="row">
		<div className="col-lg-12 col-md-12 col-xs-12">
					<div className="sub-title text-center">
						<h3>Other Services</h3>
						
					</div>
                </div>
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="0.2s">
              <div className="icon color-1">
                <img src="images/services/research.svg" alt="" />
              </div>
              <h4>New electronic work permit</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="0.4s">
              <div className="icon color-2">
              <img src="images/services/2.svg" alt="" />
              </div>
              <h4>Renew electronic work</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="0.6s">
              <div className="icon color-3">
              <img src="images/services/3.svg" alt="" />
              </div>
              <h4>Guidance lectures for workers</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="0.8s">
              <div className="icon color-4">
              <img src="images/services/4.svg" alt="" />
              </div>
              <h4>Typing new job offer letter</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="1s">
              <div className="icon color-5">
              <img src="images/services/5.svg" alt="" />
              </div>
              <h4>Submit renew labor card application</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="1.2s">
              <div className="icon color-6">
              <img src="images/services/6.svg" alt="" />
              </div>
              <h4>Updates establishment information</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="1.2s">
              <div className="icon color-6">
              <img src="images/services/7.svg" alt="" />
              </div>
              <h4>Add electronic PRO</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="1.2s">
              <div className="icon color-6">
              <img src="images/services/8.svg" alt="" />
              </div>
              <h4>Pre-approval for work permit payment fees</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="1.2s">
              <div className="icon color-6">
              <img src="images/services/9.svg" alt="" />
              </div>
              <h4>Company fines</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="1.2s">
              <div className="icon color-6">
              <img src="images/services/10.svg" alt="" />
              </div>
              <h4>New E-sign cards for owners & PRO</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="1.2s">
              <div className="icon color-6">
              <img src="images/services/11.svg" alt="" />
              </div>
              <h4>Print request for E-sign</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12">
            <div className="item-boxes services-item wow fadeInDown" data-wow-delay="1.2s">
              <div className="icon color-6">
              <img src="images/services/12.svg" alt="" />
              </div>
              <h4>Activate E-sign</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/* Services Section End */}

 {/* Start img Section */}
 <section className="video-promo section">
    
      <div className="container">
        <div className="row">
          <div className="col-md-12 col-sm-12">
             
          </div>
        </div>
      </div>
    </section>
    {/* End img Section */}

     
    {/* Portfolio Section */}
    <section id="portfolios" className="section">
      {/* Container Starts */}
      <div className="container">
        <div className="section-header">          
          <h2 className="section-title">Our clients</h2>
          <span>clients</span>
          <p className="section-subtitle hide">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos debitis.</p>
        </div>
        

        {/* Portfolio Recent Projects */}
        <div id="portfolio" className="row">
          <div className="col-lg-4 col-md-6 col-xs-12 ">
            <div className="portfolio-item">
              <div className="shot-item">
                <img src="images/clients/airolink.png" alt="" />  
                <div className="single-content">
                  <div className="fancy-table">
                    <div className="table-cell">                    
                      <a href="#">View Project</a>
                    </div>
                  </div>
                </div>
              </div>               
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12 ">
            <div className="portfolio-item">
              <div className="shot-item">
                <img src="images/clients/alnoor.png" alt="" />  
                <div className="single-content">
                  <div className="fancy-table">
                    <div className="table-cell">                    
                      <a href="#">View Project</a>
                    </div>
                  </div>
                </div>
              </div>               
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12 ">
            <div className="portfolio-item">
              <div className="shot-item">
                <img src="images/clients/aroma.png" alt="" />  
                <div className="single-content">
                  <div className="fancy-table">
                    <div className="table-cell">                    
                      <a href="#">View Project</a>
                    </div>
                  </div>
                </div>
              </div>               
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12 ">
            <div className="portfolio-item">
              <div className="shot-item">
                <img src="images/clients/binladin.png" alt="" />  
                <div className="single-content">
                  <div className="fancy-table">
                    <div className="table-cell">                    
                      <a href="#">View Project</a>
                    </div>
                  </div>
                </div>
              </div>               
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12 ">
            <div className="portfolio-item">
              <div className="shot-item">
                <img src="images/clients/dubaidrive.png" alt="" />  
                <div className="single-content">
                  <div className="fancy-table">
                    <div className="table-cell">                    
                      <a href="#">View Project</a>
                    </div>
                  </div>
                </div>
              </div>               
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-xs-12 ">
            <div className="portfolio-item">
              <div className="shot-item">
                <img src="images/clients/hill.png" alt="" />  
                <div className="single-content">
                  <div className="fancy-table">
                    <div className="table-cell">                    
                      <a href="#">View Project</a>
                    </div>
                  </div>
                </div>
              </div>               
            </div>
          </div>
        </div>
      </div>
      {/* Container Ends */}
    </section>
    {/* Portfolio Section Ends */} 

    {/* Contact Section Start */}
    <section id="contactus" className="section">      
      <div className="contact-form">
        <div className="container">
          <div className="section-header">          
            <h2 className="section-title">Get In Touch</h2>
            <span>Contact</span>
            <p className="section-subtitle hide">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos debitis.</p>
          </div>
          <div className="row">          
            <div className="col-lg-9 col-md-9 col-xs-12">
              <div className="contact-block">
                <form id="contactForm">
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <input type="text" className="form-control" id="name" name="name" placeholder="Your Name" required data-error="Please enter your name" />
                        <div className="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <input type="text" placeholder="Your Email" id="email" className="form-control" name="name" required data-error="Please enter your email" />
                        <div className="help-block with-errors"></div>
                      </div> 
                    </div>
                    <div className="col-md-12">
                      <div className="form-group">
                        <input type="text" placeholder="Subject" id="msg_subject" className="form-control" required data-error="Please enter your subject" />
                        <div className="help-block with-errors"></div>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="form-group"> 
                        {/* <textarea className="form-control" id="message" placeholder="Your Message" rows="7" data-error="Write your message" required></textarea>
                        <div className="help-block with-errors"></div> */}
                         <select className="form-control" name="message" id="message" required data-error="Please select enquiry type">
                        <option>Select Enquiry Type</option>
                        <option value="Call back">Call back</option>
                        <option value="Enquiry">Enquiry</option>
                        <option value="Booking">Booking</option>
                        <option value="Transportation">Transportation</option>
                        <option value="Complaints">Complaints</option>
                        <option value="More info">More info</option>
                        <option value="Training">Training</option>
                        <option value="Documents">Documents</option>
                        <option value="Others">Others</option>
                        
                        </select>
                        <div className="help-block with-errors"></div>
                      </div>
                      <div className="submit-button">
                        <button className="btn btn-common btn-effect" id="submit" type="submit">Send Message</button>
                        <div id="msgSubmit" className="h3 hidden"></div> 
                        <div className="clearfix"></div> 
                      </div>
                    </div>
                  </div>            
                </form>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-xs-12">
              <div className="contact-deatils">
                {/* Content Info */}
                <div className="contact-info_area">
                  <div className="contact-info">
                    <i className="lni-map"></i>
                    <h5>Location</h5>
                    <p>Showroom No. 10, Ras AL Khor complex
                       Nad Al Hamar <br/>
                      Dubai – U.A.E</p>
                  </div>
                  {/* Content Info */}
                  <div className="contact-info">
                    <i className="lni-star"></i>
                    <h5>E-mail</h5>
                    <p>info@tawjeehuae.ae</p>
                  </div>
                  {/* Content Info */}
                  <div className="contact-info">
                    <i className="lni-phone"></i>
                    <h5>Phone</h5>
                    <p>04-2880223, 04-2230889</p>
                  </div>
                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>            
    </section>
    {/* Contact Section End */}
    
    {/* Map Section Start */}
    <section id="google-map-area">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 padding-0">
            <object data="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1805.2364633018767!2d55.37124865791063!3d25.187269023165992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f671b89586b81%3A0x98bd34f360836e5!2sRas+Al+Khor+Complex+-+Dubai+-+United+Arab+Emirates!5e0!3m2!1sen!2sin!4v1554142981270!5m2!1sen!2sin"></object>
          </div>
        </div>
      </div>
    </section>
    {/* Map Section End */}

    {/* Footer Section Start */}
    <footer>
      {/* Footer Area Start */}
      <section className="footer-Content"> 
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <a  href="" className="footer-logo">
                <img src="img/logo.png" />
              </a>
              <div className="textwidget">
                <p>If you think you have the passion, 
                attitude and capability to join us 
                the next big software company
                s so that we can get the convers.</p>
              </div>
              
                <strong>Office hours:</strong>
             
              <p>From Saturday to Thursday<br/>
From 8:00 am to 8:00 PM</p>
              <ul className="footer-social">
                <li><a className="facebook" href="#"><i className="lni-facebook-filled"></i></a></li>
               
              </ul> 
            </div>
            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div className="widget">
                <h3 className="block-title">Quick Link</h3>
                <ul className="menu">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">About Us</a></li>
                  <li><a href="#">Services</a></li>
                  <li><a href="#">Contact</a></li>
                  <li><a href="#">Book Now</a></li>
                </ul>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div className="widget">
                <h3 className="block-title">Contact Us</h3>
                <ul className="contact-footer">
                  <li>
                    <strong>Address :</strong>
                    <span>Showroom No. 10, Ras AL Khor complex<br/>
                       Nad Al Hamar<br/>
                      Dubai – U.A.E</span>
                  </li>
                  <li>
                    <strong>Telephone :</strong> <span>04-2880223, 04-2230889</span>
                  </li>
                  <li>
                    <strong>E-mail :</strong> <span><a href="#">info@tawjeehuae.ae</a></span>
                  </li>
                </ul> 
              </div>
            </div>
           
          </div>
        </div>
      </section>
      {/* Footer area End */}
      
     

    </footer>
    {/* Footer Section End */} 

    {/* Go To Top Link */}
    <a href="#" className="back-to-top">
      <i className="lni-arrow-up"></i>
    </a>

    <div id="loader">
      <div className="spinner">
        <div className="double-bounce1"></div>
        <div className="double-bounce2"></div>
      </div>
    </div>    

            </div>
        )
    }
}

export default Home2;