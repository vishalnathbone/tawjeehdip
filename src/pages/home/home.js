import React, {Component} from 'react';
import $ from 'jquery';
import OwlCarousel from 'react-owl-carousel2';
//import 'react-owl-carousel2/lib/style.css'; //Allows for server-side rendering.
import 'react-owl-carousel2/src/owl.carousel.css'
import 'react-owl-carousel2/src/owl.theme.default.css'
import './home.scss';
class Home extends Component{

    render (){
        const options = {
            items: 3,
            rewind: true,
            autoplay: true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:3,
                    nav:false,
                    loop:false
                }
            }
        };
        return(
            <div className="home-page">
               
                        <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                            <ol className="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <img className="d-block w-100" src="images/slider/1.jpg" alt="First slide" />
                                    <div className="carousel-caption d-none d-md-block">
                                        <h5>First slide label</h5>
                                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <img className="d-block w-100" src="images/slider/2.jpg" alt="First slide" />
                                    <div className="carousel-caption d-none d-md-block">
                                        <h5>Second slide label</h5>
                                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <img className="d-block w-100" src="images/slider/3.jpg" alt="First slide" />
                                    <div className="carousel-caption d-none d-md-block">
                                        <h5>Third slide label</h5>
                                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                    </div>
                                </div>
                            </div>
                            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="sr-only">Previous</span>
                            </a>
                            <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="sr-only">Next</span>
                            </a>
                        </div>

                        <div className="container section-1">
                            <div className="row">
                                <div className="col-md-12">
                                    <h2>Experience that will last a lifetime</h2>
                                    <p>Tawjeeh is a semi- government institution that belongs to Ministry of Human Resources & Emiratisation (MOHRE) which was developed according to thestrategy of Emirates Programme for Excellence in Government Services to ensure high standards in the labor laws. Tawjeeh centers provide various services above which the most important serviceis guidance lectures for both employees and employers. The lectures are conducted in a uniform manner in multi-language with distribution of brochures and pamphlets.Center is well equipped with highly trained professionals to provide all required information on labor laws and regulations, submit and renew work permits, issuance of permits and labor contracts, typing new job offers.</p>
                                </div>
                            </div>
                        </div>

                         <div className="container section-2">
        
                            <div className="row">
                                <div className="col-md-12">
                                    <h2>Our Services</h2>
                                    <OwlCarousel  options={options} >
                                        <div>
                                            <div className="card">
                                                <img className="" src="images/slider/3.jpg" alt="Card image cap" />
                                                <div className="card-body">
                                                    <h5 className="card-title">New electronic work permit</h5>
                                                    <p className="card-text">Some quick example text to build on the New electronic work permit and make up the bulk of the card's content.</p>
                                                    <a href="#" className="btn">Read More</a>
                                                </div>
                                            </div>
                                        </div>

                                          <div>
                                            <div className="card">
                                                <img className="" src="images/slider/4.jpg" alt="Card image cap" />
                                                <div className="card-body">
                                                    <h5 className="card-title">New electronic work permit</h5>
                                                    <p className="card-text">Some quick example text to build on the New electronic work permit and make up the bulk of the card's content.</p>
                                                    <a href="#" className="btn">Read More</a>
                                                </div>
                                            </div>
                                        </div>

                                          <div>
                                            <div className="card" >
                                                <img className="" src="images/slider/2.jpg" alt="Card image cap" />
                                                <div className="card-body">
                                                    <h5 className="card-title">New electronic work permit</h5>
                                                    <p className="card-text">Some quick example text to build on the New electronic work permit and make up the bulk of the card's content.</p>
                                                    <a href="#" className="btn">Read More</a>
                                                </div>
                                            </div>
                                        </div>

                                          <div>
                                            <div className="card" >
                                                <img className="" src="images/slider/1.jpg" alt="Card image cap" />
                                                <div className="card-body">
                                                    <h5 className="card-title">New electronic work permit</h5>
                                                    <p className="card-text">Some quick example text to build on the New electronic work permit and make up the bulk of the card's content.</p>
                                                    <a href="#" className="btn">Read More</a>
                                                </div>
                                            </div>
                                        </div>

                                          <div>
                                            <div className="card" >
                                                <img className="" src="images/slider/4.jpg" alt="Card image cap" />
                                                <div className="card-body">
                                                    <h5 className="card-title">New electronic work permit</h5>
                                                    <p className="card-text">Some quick example text to build on the New electronic work permit and make up the bulk of the card's content.</p>
                                                    <a href="#" className="btn">Read More</a>
                                                </div>
                                            </div>
                                        </div>

                                          <div>
                                            <div className="card" >
                                                <img className="" src="images/slider/1.jpg" alt="Card image cap" />
                                                <div className="card-body">
                                                    <h5 className="card-title">New electronic work permit</h5>
                                                    <p className="card-text">Some quick example text to build on the New electronic work permit and make up the bulk of the card's content.</p>
                                                    <a href="#" className="btn">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                     
                                    </OwlCarousel>
                                </div>
                            </div>
                        </div>

                        <div className="service-grid">
                            <div className="container">
                                <div className="row">
                                    <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/permit.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>New electronic work permit</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>

                                     <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/slider/3.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>Renew electronic work</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>

                                     <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/guidance.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>Guidance lectures for workers</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>
                                    <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/typing.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>Typing new job offer letter</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>

                                     <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/labor.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>Submit renew labor card application</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>

                                     <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/establishment.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>Updates establishment information</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>

                                     <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/slider/3.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>Add electronic PRO</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>

                                     <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/slider/3.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>Pre-approval for work permit payment fees</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>

                                     <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/slider/3.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>Company fines</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>

                                    <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/slider/3.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>New E-sign cards for owners and PRO</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>
                                    <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/slider/3.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>Print request for E-sign</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>
                                    <div className="col-12 col-sm-6 col-md-3 col-lg-3 grid-content">
                                        <div className="grid">
                                            <figure className="effect-lily">
                                                <img src="images/slider/3.jpg" alt="img12"/>
                                                <figcaption>
                                                    <div>
                                                        <h2>Activate E-sign</h2>
                                                        
                                                    </div>
                                                    <a href="#">View more</a>
                                                </figcaption>			
                                            </figure>
                                          
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                  
            </div>
        )
    }
}
export default Home;